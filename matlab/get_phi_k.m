function [phi_k,CEF,q] = get_phi_k(w,v,q_0,t)

    wx = [0 -1*w(3) w(2);w(3) 0 -1*w(1);-1*w(2) w(1) 0];
    vx = [0 -1*v(3) v(2);v(3) 0 -1*v(1);-1*v(2) v(1) 0]*0;
    [CEF,q] = get_cfe(w,t,q_0);
    F = [zeros(3),CEF',zeros(3,6);zeros(3),-wx,vx,eye(3);zeros(6,12)];
    phi_k = expm(F*t);
end