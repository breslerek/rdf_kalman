function [x,P] = kalman_filter(x,P,z,F,H,R,Q)

x = F * x;
P = F * P * F' + Q;
v = z - H * x;
S = H * P * H' + R;
K = P * H' * inv(S);
x = x + K * v;
I = eye(length(x));
P = (I - K * H) * P;

end