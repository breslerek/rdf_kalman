%Script that calculates location based on barometer and gps
%% uploading data
clear all; close all;
data = readtable("2019-09-28_Fok_Telemega_conv.csv");
lat = table2array(data(:,3));
lon = table2array(data(:,4));
alt = table2array(data(:,5));
height = table2array(data(:,12));
time = table2array(data(:,2));
origin = [lat(1), lon(1), alt(1)];
[xEast,yNorth,zUp] = latlon2local(lat,lon,alt,origin);
z = [xEast';yNorth';zUp'+170;height'];
t = (abs(time(1))+time(end))/length(time);

%% environment setup
std_x = 1.2; std_y = 10; std_z = 1.3; std_h = 1; 
std_r = 1; std_v = 1; std_a = 1;
std_u = 0.0001; 
Fp = [1 t t^2/2; 0 1 t; 0 0 1];
F = blkdiag(Fp,Fp,Fp,1);
H = [1 0 0 0 0 0 0 0 0 0; 0 0 0 1 0 0 0 0 0 0; 0 0 0 0 0 0 1 0 0 0; ...
    0 0 0 0 0 0 0 0 0 1];
B = [t^2/2;t;1];
Qp = std_u^2*B*B';
Q = blkdiag(Qp, Qp, Qp,1);
R = diag([std_x^2 std_y^2 std_z^2 std_h^2]); 
x_est = [z(1,1);0;0;z(2,1);0;0;z(3,1);0;0;z(4,1)]; 
P = diag([std_x^2 std_v^2 std_a^2 std_y^2 std_v^2 std_a^2 std_z^2 ...
    std_v^2 std_a^2 std_h^2]);

%% Kalman filtration
for k=2:length(time)
    [x_est(:,k),P(:,:,k)] = kalman_filter(x_est(:,k-1),P(:,:,k-1),z(:,k),...
        F,H,R,Q); 
end
n=1:length(time);

r_gps = [x_est(1,:)',x_est(4,:)',x_est(7,:)'];
h_baro = x_est(10,:)';
save("gps_baro_data","r_gps","h_baro");
%% movement visualization
close all; 
figure(1)
plot(n,x_est(1,:), n, z(1,:), '.');
title("Ruch w osi x");
legend("x\_est","x\_data");
xlabel("[m]");
ylabel("[m]");
figure(2)
plot(n,x_est(4,:), n, z(2,:), '.');
title("Ruch w osi y");
legend("y\_est","y\_data");
xlabel("[m]");
ylabel("[m]");
figure(3)
plot(n,x_est(7,:), n, x_est(10,:), n, z(3,:), '.');
title("Ruch w osi z");
legend("z\_est","h\_est","z\_data");
xlabel("[m]");
ylabel("[m]");
figure(4)
plot3(x_est(1,:),x_est(4,:),x_est(7,:));
title("Wizualizacja 3d");
xlabel("[m]");
ylabel("[m]");
zlabel("[m]");




