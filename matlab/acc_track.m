%% uploading data
clear all; close all; clc;
data = readtable("2019-09-28_Fok_Telemega_conv.csv");
lat = deg2rad(table2array(data(:,3)));
lon = deg2rad(table2array(data(:,4)));
alt = table2array(data(:,5));
time = table2array(data(:,2));
a_x = table2array(data(:,6));
a_y = table2array(data(:,7));
a_z = table2array(data(:,8));
g_x = deg2rad(table2array(data(:,9))); 
g_y = deg2rad(table2array(data(:,10))); 
g_z = deg2rad(table2array(data(:,11)));
z = [a_x,a_y,a_z,g_x,g_y,g_z];
t = 0.01;
%% initial conditions
g = 9.81;
r0 = lla2ecef([rad2deg(lat(1)),rad2deg(lon(1)),alt(1)]);
v0 = [0,0,0];
theta_0 = asin(a_x(1)/g);
psi_0 = deg2rad(109);
phi_0 = asin(a_y(1)/(g*cos(theta_0)));
CNE_1 = [-1*sin(lat(1))*cos(lon(1)), -1*sin(lon(1)), -1*cos(lat(1))*cos(lon(1))];
CNE_2 = [-1*sin(lat(1))*sin(lon(1)), cos(lon(1)), -1*cos(lat(1))*sin(lon(1))];
CNE_3 = [cos(lat(1)),0,-1*sin(lat(1))];
CNE = [CNE_1;CNE_2;CNE_3];
CNF = angle2dcm(psi_0,theta_0,phi_0);
CEF = CNF*CNE';
q_0 = dcm2quat(CEF);

%% movement visualization
std_x = 1; std_y = 1; std_z = 1;
std_r = 1; 
std_vx = 1;  std_vy = 1;  std_vz = 1;
std_ax = 0.05;  std_ay = 1;  std_az = 1;
std_gx = 0.01;  std_gy = 0.25;  std_gz = 0.2;
std_u = 0.0001;
w0= [g_x(1);g_y(1);g_z(1)];
wx = [0 -1*w0(3) w0(2);w0(3) 0 -1*w0(1);-1*w0(2) w0(1) 0];
vx = zeros(3,3);
F = [zeros(3),CEF',zeros(3,6);zeros(3),-wx,vx,eye(3);zeros(6,12)];
phi_k = expm(F*t);
H = [zeros(3,9),eye(3);zeros(3,6),eye(3),zeros(3)];
Q = eye(12,12)*std_u;
R = diag([std_ax^2 std_ay^2 std_az^2 std_gx^2 std_gy^2 std_gz^2]); 
% P = diag([std_x^2 std_y^2 std_z^2 std_vx^2 std_vy^2 std_vz^2 ...
%     std_gx^2 std_gy^2 std_gz^2 std_ax^2 std_ay^2 std_az^2]);
P = 1*eye(12);
x_est = [r0';v0';0;0;0;0;0;0]; 
z(1,1:3) = z(1,1:3) + [CNF*[0,0,g]']';

%% Kalman filtration
for k=2:length(time)
    pomiar = z(k-1,:)';
    [x_est,P] = kalman_filter(x_est,P,pomiar,phi_k,H,R,Q); 
    x(k,:) = x_est;
    w = [z(k,4) z(k,5) z(k,6)];
    v = [x_est(4) x_est(5) x_est(6)];
    [phi_k, CEF, q_0] = get_phi_k(w,v,q_0,t);
    lla = ecef2lla([x_est(1) x_est(2) x_est(3)]);
    latt = deg2rad(lla(1));
    lonn = deg2rad(lla(2));
    CNE_1 = [-1*sin(latt)*cos(lonn), -1*sin(lonn), -1*cos(latt)*cos(lonn)];
    CNE_2 = [-1*sin(latt)*sin(lonn), cos(lonn), -1*cos(latt)*sin(lonn)];
    CNE_3 = [cos(latt),0,-1*sin(latt)];
    CNE = [CNE_1;CNE_2;CNE_3];
    CNF = CEF*CNE;
    z(k,1:3) = z(k,1:3) + [CNF*[0,0,g]']';
    r_wz(k,:) = CNE'*(x_est(1:3)-r0');
end
n=1:length(time);
save("wsp_imu","r_wz")

%% verification
close all; 
test = 1:2700;
% figure(1)
% plot(test,rad2deg(x(test,7)),test,rad2deg(z(test,4)),'.');
% xlabel('Sample')
% ylabel('omega_x[deg/s]')
% title('omega_x')

% figure(2)
% plot(test,rad2deg(x(test,8)),test,rad2deg(z(test,5)),'.');
% xlabel('Sample')
% ylabel('omega_y[deg/s]')
% title('omega_y')
% 
% figure(3)
% plot(test,rad2deg(x(test,9)),test,rad2deg(z(test,6)),'.');
% xlabel('Sample')
% ylabel('omega_z[deg/s]')
% title('omega_z')

% figure(4)
% plot(test,x(test,10),test,z(test,1),'.');
% xlabel('Sample')
% ylabel('a_x[m/s^2]')
% title('a_x')
% 
% figure(5)
% plot(test * t,x(test,11),test * t,z(test,2),'.');
% xlabel('Sample')
% ylabel('a_y[m/s^2]')
% title('a_y')
% 
% figure(6)
% plot(test * t,x(test,12),test * t,z(test,3),'.');
% xlabel('Sample')
% ylabel('a_z[m/s^2]')
% title('a_z')
% 
% figure(7)
% plot(test,x(test,4));
% xlabel('Sample')
% ylabel('v_x[m/s]')
% title('v_x')
% 
% figure(8)
% plot(test,x(test,5));
% xlabel('Sample')
% ylabel('v_y[m/s]')
% title('v_y')
% 
% figure(9)
% plot(test,x(test,6));
% xlabel('Sample')
% ylabel('v_z[m/s]')
% title('v_z')
% 
% figure(10)
% plot3(r_wz(test,1),r_wz(test,2),-r_wz(test,3));
% % plot(sqrt(r_wz(:,1).^2+r_wz(:,2).^2),-r_wz(:,3));
% axis('equal');
% % xlabel('Zasieg')
% ylabel('Wysokosc')
% title('Trajektoria')figure(11)
plot(n,r_wz(:,1))
title("Ruch w osi x");
xlabel("[m]");
ylabel("[m]");

figure(12)
plot(n,r_wz(:,2))
title("Ruch w osi y");
xlabel("[m]");
ylabel("[m]");

figure(13)
plot(n,-r_wz(:,3))
title("Ruch w osi z");
xlabel("[m]");
ylabel("[m]");




