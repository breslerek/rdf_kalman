%% uploading data
clear all; close all;
load("wsp_imu");
load("gps_baro_data");
% apogee - aounrd 2600 sample

%% calculation of vectors
n = 1:length(r_wz);
apo = 2000;
tmp = 1:apo;
std_gps_x = 1.2; std_gps_y = 1.5; std_gps_z = 1.2;
std_imu_x = 2; std_imu_y = 2; std_imu_z = 2.0654;
std_baro_h = 1.8;
z = (r_gps(:,3)/std_gps_z^2 + h_baro/std_baro_h^2)*(std_gps_z^2 + std_baro_h^2)/(std_gps_z^2 * std_baro_h^2);
z_imu_gps = (r_gps(tmp,3)/std_gps_z^2 + (-r_wz(tmp,3)+170)/std_imu_z^2)*(std_gps_z^2 + std_imu_z^2)/(std_gps_z^2 * std_imu_z^2);
z = [z_imu_gps;z(apo+1:end)];
y = (r_gps(:,2)/std_gps_y^2 + (-r_wz(:,2))/std_imu_y^2)*(std_gps_y^2 + std_imu_y^2)/(std_gps_y^2 * std_imu_y^2);
x = (r_gps(:,1)/std_gps_x^2 + r_wz(:,1)/std_imu_x^2)*(std_gps_x^2 + std_imu_x^2)/(std_gps_x^2 * std_imu_x^2);

%% visualization
figure(1)
plot(n,r_gps(:,1), n, r_wz(:,1),n,x);
title("Ruch w osi x");
legend("x\_gps","x\_imu","x\_fusion");
figure(2)
plot(n,r_gps(:,2), n, -r_wz(:,2), n ,y);
title("Ruch w osi y");
legend("y\_gps","y\_imu","y\_fusion");
figure(3)
plot(n,r_gps(:,3), n, h_baro, n, (-r_wz(:,3)+170), n, z);
title("Ruch w osi z");
legend("z\_gps","z\_baro", "z\_imu","z\_fusion");
% figure(4)
% plot3(x,y,z);
% title("Wizualizacja 3d");

% tiledlayout(2,2)
% nexttile
% plot(n,x);
% title("Ruch w osi x");
% xlabel("[m]");
% ylabel("[m]");
% nexttile
% plot(n,y);
% title("Ruch w osi y");
% xlabel("[m]");
% ylabel("[m]");
% nexttile
% plot(n,z);
% title("Ruch w osi z");
% xlabel("[m]");
% ylabel("[m]");
% nexttile
% plot3(x,y,z);
% title("Wizualizacja 3d");
% xlabel("[m]");
% ylabel("[m]");