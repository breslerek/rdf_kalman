function [CEF,q] = get_cfe(w,t,q_0)

    xi = norm(w)*t;
    r1 = q_0(1); r2 = cos(xi/2); v1 = q_0(2:4); v2 = w/norm(w) * sin(xi/2);
    q = [r1*r2-dot(v1,v2) r1*v2+r2*v1+cross(v1,v2)];
    CEF1 = [q(1)^2+q(2)^2-q(3)^2-q(4)^2 2*(q(2)*q(3)+q(1)*q(4)) 2*(q(2)*q(4)-q(1)*q(3))];
    CEF2 = [2*(q(2)*q(3)-q(1)*q(4)) q(1)^2-q(2)^2+q(3)^2-q(4)^2 2*(q(3)*q(4)+q(1)*q(2))];
    CEF3 = [2*(q(2)*q(4)+q(1)*q(3)) 2*(q(3)*q(4)-q(1)*q(2)) q(1)^2-q(2)^2-q(3)^2+q(4)^2];
    CEF = [CEF1;CEF2;CEF3];

end