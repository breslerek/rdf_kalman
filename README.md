# Rdf Kalman

Rocket flight data fusion with Kalman filtering. Project realised for Radiolocation Signals and Methods of their Processing lecture at Warsaw University of Technology. Full project report is available only in Polish. 

The aim of the project was to filter raw data from GPS and IMU located on FOK rocket during its 3rd flight. After using Kalman to process the data, simple data fusion was carried out to merge avaiable data.

To run the programs, open Matlab (at leat 2020b) and run gps_track.m -> acc_track.m -> data_fusion.m. You can generate different plots by uncommenting corresponding lines of code in visualization sections. Derived equations of motion are available in report .pdf and .docx file (great thanks to Darek Miedziński, who helped me a lot with their derivation!). 

Example results on Z axis filtration (altitude):

![](z_data.png)
